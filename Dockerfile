FROM photon:4.0

RUN tdnf install -y rpm
RUN tdnf -y --downloadonly --downloaddir=/tmp install httpd
RUN tdnf install -y apr-util
RUN rpm2cpio /tmp/httpd-*.rpm | cpio -iuvdm /usr/bin/htpasswd
RUN rm -f /tmp/*
RUN rm -rf /usr/sbin/httpd

LABEL author=stonezdj 